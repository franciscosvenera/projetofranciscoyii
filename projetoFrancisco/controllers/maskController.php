<?
namespace app\controllers;
use yii\web\Controller;

Class maskController extends Controller{

    public static function mask($val = '', $format){
        $maskared = '';
        $k = 0;
        switch ($format) {
            case 'cpf': 
                $mask = '###.###.###-##';
                break;
            case 'cnpj':
                $mask = '##.###.###/####-##';
                break;
            case 'cep':
                $mask = '#####-###';
                break;
            default:
                $mask = null;
                break;
        }
        if($val == null){
            return "--";
        }
        for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else if (isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
    
        return $maskared;
    }
}
?>