<?

use app\controllers\ConsultasController;
use yii\helpers\Html;

?>

<div class="row mb-4">
    <div class="col-12 text-center">
        <img src="img/homeImg.svg" alt="" style="width: 170px;" class="mt-4" >
    </div>
    <div class="col-12 text-center"><h1 class="text-center">Dashboard</h1></div>
</div>
<body>
    <main>
        <div class="row col-sm-12">
            <div class="col-md-6">
                <div class="card mx-auto shadow rounded text-center p-4 my-4" style="background-color: #e7e7e7;">
                <h5 class="card-title">Moradores por condomínio</h5>
                    <?$moradores = ConsultasController::getCondMoradores();
                    
                    foreach ($moradores as $value){?>
                    <ul class="list-group col-sm-12">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <?=$value['nomeCondominio']?>
                                <span class=""><?=$value['Total']?></span>
                            </li>
                        </ul>
                    <?}?>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="card mx-auto shadow rounded text-center p-4 my-4" style="background-color: #e7e7e7;">
                    <h5 class="card-title">Últimas 5 administradoras</h5>
                    <?
                    $condominios = ConsultasController::getLastAdm();
                    foreach ($condominios as $value) {
                    ?>
                        <ul class="list-group col-sm-12">
                            <li class="list-group-item"><?=$value['nomeAdm']?></li>
                        </ul>
                    <?}?>
                </div>
            </div>
        </div>
        <br>
        <div class="row mb-12">
            <div class="col-md-6">
                <div class="card mx-auto shadow rounded text-center p-4 my-4"  style="background-color: #e7e7e7;">
                    <p class="pt-2 h4">Cadastrar</p>
                    <li><a href="index.php?r=administradoras/cadastrar-administradoras" class="text-dark">Administradoras</a></li>
                    <li><a href="index.php?r=condominios/cadastrar-condominios" class="text-dark">Condominios</a></li>
                    <hr>
                    <p class="pt-2 h4">Listar</p>
                    <li><a href="index.php?r=administradoras/listar-administradoras" class="text-dark">Administradoras</a></li>
                    <li><a href="index.php?r=condominios/listar-condominios" class="text-dark">Condominios</a></li>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="card mx-auto shadow rounded text-center p-4 my-4" style="background-color: #e7e7e7;">
                    <p class="pt-2 h4">Cadastrar</p>
                    <li><a href="index.php?r=moradores/cadastrar-moradores" class="text-dark">Moradores</a></li>
                    <li><a href="index.php?r=conselhos/cadastrar-conselhos" class="text-dark">Conselho fiscal</a></li>
                    <hr>
                    <p class="pt-2 h4">Listar</p>
                    <li><a href="index.php?r=moradores/listar-moradores" class="text-dark">Moradores</a></li>
                    <li><a href="index.php?r=conselhos/listar-conselhos" class="text-dark">Conselho fiscal</a></li>
                </div> 
            </div>
        </div>
    
    </main>
</body>

<hr>


<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
