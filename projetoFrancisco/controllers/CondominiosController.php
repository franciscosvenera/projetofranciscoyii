<?
namespace app\controllers;

use app\models\AdministradorasModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominiosModel;
use app\models\ConselhosModel;
use yii;

class CondominiosController extends Controller{
    public function actionListarCondominios(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        //$query = CondominiosModel::find();
        $condTable = CondominiosModel::tableName();
        $admTable = AdministradorasModel::tableName();
        $consTable = ConselhosModel::tableName();
        $query = (new \yii\db\Query())
        ->select(
        'cond.id,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtde,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.uf,
        cond.sindico,
        cond.idAdmin,
        cons.nome'

        )
        ->from($condTable.' cond')
        ->innerJoin($admTable.' adm', 'adm.id = cond.idAdmin')
        ->innerJoin($consTable.' cons', 'cons.id = cond.sindico');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $condominios = $query->orderBy('nomeCondominio')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-condominios',[
            'condominios' => $condominios,
            'paginacao' => $paginacao,
        ]);
    }
    public function actionCadastrarCondominios(){
        return $this->render('cadastrar-condominios');
    }

    public function actionRealizaCadastroCondominio(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new CondominiosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['condominios/listar-condominios']);
        }
        return $this->render('cadastrar-condominios');
    }

    public static function listaCondominiosSelect(){
        $query = CondominiosModel::find();

        return $query->orderBy('nomeCondominio')->all();
    }

    public function actionListCondominiosApi(){
        $request = \yii::$app->request;
        $query=CondominiosModel::find();
        $data = $query->where(['idCondominio' => $request->post()])->orderBy('nomeCondominio')->all();

        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeCondominio'] = $d['nomeCondominio'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditarCondominios(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = CondominiosModel::find();
            $condominios = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-condominios',[
        'edit' => $condominios
        ]);
      }
  
      public function actionRealizaEdicaoCondominio(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = CondominiosModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['condominios/listar-condominios',
                  'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'administradoras/listar-administradoras']
                ]);
              }else{
                  return $this->redirect(['condominios/listar-condominios', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaCondominio(){
        
        try {
            $request = \yii::$app->request;
            $model = CondominiosModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['condominios/listar-condominios', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'condominios/listar-condominios'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['condominios/listar-condominios', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'condominios/listar-condominios'
                ]
            ]);  
        }

    }
}
?>