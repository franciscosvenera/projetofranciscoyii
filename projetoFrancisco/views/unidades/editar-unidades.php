<?

use app\components\selectedComponents;
use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
?>

<h1>Editar Unidades</h1>
<form method="post" id="cadastroUnidade" action="<?= Url::to(['unidades/realiza-edicao-unidade']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Número unidade</label>
            <input class="form-control" type="text" name="numeroUnidade" value="<?=$edit['numeroUnidade']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">Metragem</label>
            <input class="form-control" type="text" name="metragem" value="<?=$edit['metragem']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Vagas garagem</label>
            <input class="form-control" type="text" name="vagasGaragem" value="<?=$edit['vagasGaragem']?>" required>
        </div>

        <div class="col-sm-4 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select">
                <? 
                foreach(CondominiosController::listaCondominiosSelect($edit['idCondominio']) as $und){
                echo '<option value="'.$und['id'].'"'.selectedComponents::isSelected($und['id'], $edit['id']).'>'.$und['nomeCondominio'].'</option>';
            }?>
            </select> 

        </div>


        <div class="col-sm-4 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select">
                <? 
                foreach(BlocosController::listaBlocosSelect($edit['idBloco']) as $blc){
                echo '<option value="'.$blc['id'].'"'.selectedComponents::isSelected($blc['id'], $edit['id']).'>'.$blc['nomeBloco'].'</option>';
            }?>
            </select> 

        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>