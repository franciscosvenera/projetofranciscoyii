<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="img/logo.png" width="100px"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            [
                'label' => 'Administradoras',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['administradoras/listar-administradoras']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['administradoras/cadastrar-administradoras']
                    ]
                ]
            ],
            [
                'label' => 'Condominios',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['condominios/listar-condominios']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['condominios/cadastrar-condominios']
                    ]
                ]
            ],
            [
                'label' => 'Blocos',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['blocos/listar-blocos']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['blocos/cadastrar-blocos']
                    ]
                ]
            ],
            [
                'label' => 'Unidades',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['unidades/listar-unidades']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['unidades/cadastrar-unidades']
                    ]
                ]
            ],
            [
                'label' => 'Moradores',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['moradores/listar-moradores']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['moradores/cadastrar-moradores']
                    ]
                ]
            ],
            [
                'label' => 'Conselhos',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['conselhos/listar-conselhos']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['conselhos/cadastrar-conselhos']
                    ]
                ]
            ],
            [
                'label' => 'Patrimonios',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['patrimonios/listar-patrimonios']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['patrimonios/cadastrar-patrimonios']
                    ]
                ]
            ],
        Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->nome . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )    
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 mt-5">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted bg-dark">
    <div class="container">
        <p class="float-left">&copy; AP Controle <?= date('Y') ?></p>
        <p class="float-right">Suporte (47)98899-8989</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
