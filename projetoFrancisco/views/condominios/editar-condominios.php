<?

use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\controllers\ConselhosController;
use app\components\estadosComponents;
use app\components\selectedComponents;

?>

<h1>Cadastrar Condomínio</h1>
<form method="post" id="cadastroCondominio" action="<?echo Url::to(['condominios/realiza-editar-condominio']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="idAdmin">Administradora</label>
            <select name="idAdmin" class="custom-select">
                <? 
                foreach(AdministradorasController::listaAdministradorasSelect($edit['idAdmin']) as $adm){
                echo '<option value="'.$adm['id'].'"'.selectedComponents::isSelected($adm['id'], $edit['id']).'>'.$adm['nomeAdm'].'</option>';
                } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nomeCondominio" value="<?=$edit['nomeCondominio']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="qtde">Qtde. Blocos</label>
            <input class="form-control" type="text" name="qtde" value="<?=$edit['qtde']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="logradouro">Logradouro</label>
            <input class="form-control" type="text" name="logradouro" value="<?=$edit['logradouro']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="numero">Nr.</label>
            <input class="form-control" type="text" name="numero" value="<?=$edit['numero']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="bairro">Bairro</label>
            <input class="form-control" type="text" name="bairro" value="<?=$edit['bairro']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="cidade">Cidade</label>
            <input class="form-control" type="text" name="cidade" value="<?=$edit['cidade']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="uf">UF</label>
            <select name="from_estado" class="form-control fromCondominio">
                <option selected>Selecione...</option>
                <? foreach (estadosComponents::estados() as $sig => $uf) { ?>
                    <option value="<?= $sig?>"><?=$uf?></option>
                <? } ?>
            </select>    
        </div>


        <div class="col-sm-6 form-group">
            <label for="cep">CEP</label>
            <input class="form-control" type="text" name="cep" value="<?=$edit['cep']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="sindico">Sindico</label>
            <select name="sindico" class="custom-select">
                <? 
                foreach(ConselhosController::listaConselhossSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nome']?></option>
                <? } ?>
            </select>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>
