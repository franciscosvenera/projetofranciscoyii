<?
namespace app\models;

use yii\db\ActiveRecord;

class AdministradorasModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_administradoras';
    }

    public function rules(){
        return [
            [['nomeAdm', 'nomeAdm'], 'required'],
            [['CNPJ', 'CNPJ'], 'required']
        ];
    }
}
?>