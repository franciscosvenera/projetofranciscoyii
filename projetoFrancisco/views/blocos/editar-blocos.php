<?

use app\components\selectedComponents;
use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<h1>Editar blocos</h1>
<form method="post" id="cadastroBloco" action="<?= Url::to(['blocos/realiza-edicao-bloco']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Bloco</label>
            <input class="form-control" type="text" name="nomeBloco" value="<?=$edit['nomeBloco']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">Andares</label>
            <input class="form-control" type="text" name="andares" value="<?=$edit['andares']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Unid. por Andar</label>
            <input class="form-control" type="text" name="unidadesAndar" value="<?=$edit['unidadesAndar']?>" required>
        </div>

        <div class="col-sm-4 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select">
                <? 
                foreach(CondominiosController::listaCondominiosSelect($edit['idCondominio']) as $und){
                echo '<option value="'.$und['id'].'"'.selectedComponents::isSelected($und['id'], $edit['id']).'>'.$und['nomeCondominio'].'</option>';
            }?>
            </select> 

        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">
        
        <input type="hidden" name="id" value="<?=$edit['id']?>">
        
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>