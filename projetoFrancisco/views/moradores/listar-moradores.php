<?
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\maskComponents;
use app\components\alertComponents;
use app\components\modalComponents;

$url_site = Url::base(true);

if(isset($_GET['msg'])){
    echo alertComponents::myAlert('danger','Erro na requisição');
}

?>
<h1>Moradores</h1>


<table class="col col-12 table table-striped mt-5" id="listaMoradores">
    <tr>
        <td>Nome</td>
        <td>CPF</td>
        <td>E-mail</td>
        <td>Fone</td>
        <td>Condomínio</td>
        <td>Bloco</td>
        <td>Unidade</td>
        <td>Data atual.</td>
        <td>Data Cadastro</td>
        <td align="center"><a href="index.php?r=moradores/cadastrar-moradores" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($moradores as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= maskComponents::mask($dados['cpf'],'cpf')?></td>
            
            <td><?= $dados['email'] ?></td>
            <td><?=maskComponents::mask($dados['fone'],'fone') ?></td>
            <td><?= $dados['nomeCondominio'] ?></td>
            <td><?= $dados['nomeBloco'] ?></td>
            <td><?= $dados['numeroUnidade'] ?></td>
            <td><?= Yii::$app->formatter->format($dados['dataUpdate'],'date') ?></td>
            <td><?= Yii::$app->formatter->format($dados['dataCadastro'],'date') ?></td>
            <td align="center">
                <a href="index.php?r=moradores/editar-moradores&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=$url_site?>/index.php?r=moradores/deleta-morador&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>
    <?=modalComponents::initModal();?>