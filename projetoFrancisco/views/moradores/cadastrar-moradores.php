<?


use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use app\controllers\UnidadesController;
use kartik\datetime\DateTimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;

?>

<h1>Cadastra Moradores</h1>
<form method="post" id="cadastroMorador" action="<?= Url::to(['moradores/realiza-cadastro-morador']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cpf">CPF</label>
            <input class="form-control" type="text" name="cpf" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cpf">Nascimento</label>
            <?
            echo DatePicker::widget([
                'bsVersion' => '4',
                'name' => 'dataNascimento',
                'type' => DateTimePicker::TYPE_INPUT,
                'value' => '01-01-1980',
                'language' => 'pt',
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose'=>true,
                    'format' => 'dd-M-yyyy'
                ]
            ]);
            ?>
        </div>

        <div class="col-sm-6 form-group">
            <label for="email">E-mail</label>
            <input class="form-control" type="text" name="email" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="fone">Fone</label>
            <input class="form-control" type="text" name="fone" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select idCondominio">
                <? 
                foreach(CondominiosController::listaCondominiosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeCondominio']?></option>
            
                <? } ?>
            </select>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="idBloco">Blocos</label>
            <select name="idBloco" class="custom-select">
                <? 
                foreach(BlocosController::listaBlocosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeBloco']?></option>
                <? } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="idUnidade">Unidade</label>
            <select name="idUnidade" class="custom-select">
                <? 
                foreach(UnidadesController::listaUnidadesSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['numeroUnidade']?></option>
                <? } ?>
            </select>
        </div>
        <div class="col-sm-12 form-group">
            <?
            echo FileInput::widget([
                'name' => 'files',
                'attribute' => 'files[]',
                'options' => ['multiple' => true]
            ]);
            ?>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>
