<?

use app\components\alertComponents;
use app\components\modalComponents;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$url_site = Url::base($schema = true);

if(isset($_GET['msg'])){
    echo alertComponents::myAlert('danger','Erro na requisição');
}
?>
<h1>Blocos</h1>

<table class="col col-12 table table-striped mt-5" id="listaBlocos">
    <tr>
        <td>Nome</td>
        <td>Andares</td>
        <td>Unid. Andar</td>
        <td>Condominio</td>
        <td align="center"><a href="index.php?r=blocos/cadastrar-blocos" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($blocos as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nomeBloco'] ?></td>
            <td><?= $dados['andares'] ?></td>
            <td><?= $dados['unidadesAndar'] ?></td>
            <td><?= $dados['nomeCondominio'] ?></td>
            <td align="center">
            <a href="index.php?r=blocos/editar-blocos&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
            <a href="<?=$url_site?>/index.php?r=blocos/deleta-bloco&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>

    <?=modalComponents::initModal();?>