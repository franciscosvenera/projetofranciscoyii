<?
use yii\helpers\Url;
?>

<h1>Cadastra administradora</h1>
<form method="post" id="cadastroAdministradora" action="<?= Url::to(['administradoras/realiza-cadastro-administradora']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Administradora</label>
            <input class="form-control" type="text" name="nomeAdm" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">CNPJ</label>
            <input class="form-control" type="text" name="CNPJ" value="" required>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>