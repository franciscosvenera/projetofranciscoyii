<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
?>

<h1>Cadastra Unidades</h1>
<form method="post" id="cadastroUnidade" action="<?= Url::to(['unidades/realiza-cadastro-unidade']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Número unidade</label>
            <input class="form-control" type="text" name="numeroUnidade" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">Metragem</label>
            <input class="form-control" type="text" name="metragem" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Unid. por Andar</label>
            <input class="form-control" type="text" name="unidadesAndar" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Vagas garagem</label>
            <input class="form-control" type="text" name="vagasGaragem" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select idCondominio">
                <? 
                foreach(CondominiosController::listaCondominiosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeCondominio']?></option>
                <? } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="idBloco">Blocos</label>
            <select name="idBloco" class="custom-select">
                <? 
                foreach(BlocosController::listaBlocosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeBloco']?></option>
                <? } ?>
            </select>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>