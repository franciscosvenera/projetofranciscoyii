<?
use yii\helpers\Url;
use app\components\selectedComponents;
?>

<h1>Editar administradora</h1>
<form method="post" id="cadastroAdministradora" action="<?= Url::to(['administradoras/realiza-edicao-administradora']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Administradora</label>
            <input class="form-control" type="text" name="nomeAdm" value="<?=$edit['nomeAdm']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">CNPJ</label>
            <input class="form-control" type="text" name="CNPJ" value="<?=$edit['CNPJ']?>" required>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
</form>