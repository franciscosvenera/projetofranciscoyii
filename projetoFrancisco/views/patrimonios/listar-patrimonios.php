<?

use app\components\alertComponents;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\maskComponents;
use app\components\modalComponents;

$url_site = Url::base($schema = true);
if(isset($_GET['msg'])){
    echo alertComponents::myAlert('danger','Erro na requisição');
}

?>
<h1>Patrimônios</h1>

<table class="col col-12 table table-striped mt-5" id="listaPatrimonios">
    <tr>
        <td>Descrição</td>
        <td>Data aquisição</td>
        <td>Quantidade</td>
        <td>Valor</td>
        <td>Documento</td>
        <td>Observação</td>
        <td>Condominio</td>
        
        <td align="center"><a href="index.php?r=patrimonios/cadastrar-patrimonios" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($patrimonios as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['descricao'] ?></td>
            <td><?= Yii::$app->formatter->format($dados['data_aquisicao'],'date') ?></td>
            <td><?= $dados['quantidade'] ?></td>
            <td><?= Yii::$app->formatter->asCurrency($dados['valor']) ?></td>
            <td><?= $dados['documento'] ?></td>
            <td><?= $dados['observacao'] ?></td>
            <td><?= $dados['nomeCondominio'] ?></td>
            
            <td align="center">
                <a href="index.php?r=patrimonios/editar-patrimonios&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=$url_site?>/index.php?r=patrimonios/deleta-patrimonio&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>
    <?=modalComponents::initModal();?>