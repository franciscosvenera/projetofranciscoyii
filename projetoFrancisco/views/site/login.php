<?

use app\components\alertComponents;
use yii\helpers\Url;

$url_site = Url::base(true);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Acesso</title>
</head>

<body class="bg-dark">
    <main class="container">
    <?=(isset($_GET['myAlert'])) ? alertComponents::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'],$_GET['myAlert']['redir']) : '';?>
        <div class="row mt-5">
            <div class="caixaLogin" style="margin-left: 40%; margin-top: 100px;">
                <form class="form-signin" method="POST" action="<?=Url::to(['site/login']);?>">
                    <img class="mb-4 ml-3" src="img/logo.png" alt="" width="150px" >
                    <h4 class="mb-3 font-weight-normal text-light ml-3">Efetue seu login</h4>
                    <label for="usuario" class="sr-only">Usuário</label>
                    <input type="text" id="usuario" class="form-control" name="usuario" placeholder="Usuário" required autofocus>
                    <br>
                    <label for="senha" class="sr-only">Senha</label>
                    <input type="password" id="senha" class="form-control" name="senha" placeholder="Senha" required>
                    
                    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">
                    <br>
                    <button class="btn btn-lg btn-light btn-block" type="submit">Acessar</button>
                </form>
            </div>
        </div>
    </main>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/app.js"></script>
</body>

</html>