<?
namespace app\models;

use yii\db\ActiveRecord;

class UnidadesModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_unidades';
    }
    public function rules(){
        return [
            [['numeroUnidade', 'metragem', 'vagasGaragem', 'idBloco', 'idCondominio'], 'required']
        ];
    }
}
?>