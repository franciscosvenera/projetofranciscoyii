$(function(){

    $('li.dropdown').hover(function(){
        console.log('teste')
        $(this).find('.dropdown-menu').stop(true,true).toggle('show');
    }, function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('hide');
    })
  
  
    //chamar valores para selects filhos
    $(document).on('change','.idCondominio',function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: '?r=blocos/list-blocos-api',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                console.log(data)
                selectPopulation('.idBloco',data,'nomeBloco');
            }
        })

    })

    //chamar unidades após selecionar blocos
    $(document).on('change','idBloco',function(){
        selecionado = $(this).val();

        $.ajax({
            url: '?r=unidades/list-unidades-api',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success: function(data){
                console.log(data)
                selectPopulation('.idUnidade', data, 'numeroUnidade');
            }
        })
    })

    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';
        
        for (let i = 0; i < dados.length; i++) {
        
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
            
        }
        $(seletor).html(estrutura)
    }

    $("#cadastroUsuario").submit(function(){
        var senha = $(this).find('#senha').val();
        var confirmaSenha = $(this).find('#csenha').val();

        if(senha == confirmaSenha){
            $.ajax({
                url: url_site+'api/cadastraUsuario.php',
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data){
                    if(data.status == 'success'){
                        myAlert(data.status, data.msg, 'main');
                    }else{
                        myAlert(data.status, data.msg, 'main');
                    }
                }
            })
        }else{
            document.myAlert('danger','A senha digitada no campo de confirmação não confere!', 'main');
        }

        return false;
    })

    //controlador do filtro
    // $('#filtro').submit(function(){
    //     var pagina = $('input[name="page"]').val();
    //     var termo1 = $('.termo1').val();
    //     var termo2 = $('.termo2').val();

    //     termo1 = (termo1) ? termo1+'/' : '';
    //     termo2 = (termo2) ? termo2+'/' : ''

    //     window.location.href = url_site+pagina+'/busca/'+termo1+termo2

    //     return false;
    // })

    
    $('.actionGenero').change(function(){
        var gen = $(this).val();
        var name = $(this).attr('id');
        if(gen == 'O'){
            $(this).attr('name','x');
            $(this).parent().append('<input class="form-control outroGenero" type="text" name="'+name+'">');
        }else{
            $('.outroGenero').remove();
            $(this).attr('name',name);
        }
    })

 
});
    

    $('.openModal').click(function(){
        caminho = $(this).attr('href');
        $(".modal-body").load(caminho, function(response, status){

            if (status === "success"){
                $('#modalComponent').modal({show: true});
            }
        });
        return false;
    })
    

// $('.termo1, .termo2').on('keyup focusout change',function(){
//     var termo1 = $('.termo1').val();
//     var termo2 = $('.termo2').val();
//     if(termo1 || termo2){
//         $('button[type="submit"]').prop('disabled', false);
//     }else{
//         $('button[type="submit"]').prop('disabled', true);
//     }
    
// });

//mascaras 
$('input[name="cpf"]').mask('999.999.999-99', {reverse:true});

var options =  {
    onKeyPress: function(num, e, field, options) {
        console.log(num.length)
      var masks = ['(00) 0000-0000', '(00) 00000-0000'];
      var mask = (num.length > 14) ? masks[0] : masks[1];
  }};

$('input[name="telefone"]').mask('(99) 99999-9999', options);

function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function(){
        $(pai).find('div.alert').remove();
        //vai redirecionar?
        if(tipo == 'success' && url){
            setTimeout(function(){
                window.location.href = url;
            },500);
        }
    },2000)

}


