<?
namespace app\models;

use yii\db\ActiveRecord;

class BlocosModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_blocos';
    }

    public function rules(){
        return [
            [['nomeBloco', 'andares', 'unidadesAndar', 'idCondominio'], 'required']
        ];
    }
}
?>