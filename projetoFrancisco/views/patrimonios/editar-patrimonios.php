<?

use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\components\selectedComponents;

?>

<h1>Editar patrimônio</h1>
<form method="post" id="cadastroPatrimonio" action="<?echo Url::to(['patrimonios/realiza-edicao-patrimonio']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="id">Patrimônio</label>
            <select name="id" class="custom-select">
                <? 
                foreach(CondominiosController::listaCondominiosSelect($edit['id']) as $adm){
                echo '<option value="'.$adm['id'].'"'.selectedComponents::isSelected($adm['id'], $edit['id']).'>'.$adm['nomeCondominio'].'</option>';
                } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="descricao">Descricao</label>
            <input class="form-control" type="text" name="descricao" value="<?=$edit['descricao']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="data_aquisicao">Data Aquisição</label>
            <input class="form-control" type="text" name="data_aquisicao" value="<?=$edit['data_aquisicao']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="quantidade">Quantidade</label>
            <input class="form-control" type="text" name="quantidade" value="<?=$edit['quantidade']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="valor">Valor</label>
            <input class="form-control" type="text" name="valor" value="<?=$edit['valor']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="documento">Documento</label>
            <input class="form-control" type="text" name="documento" value="<?=$edit['documento']?>" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="observacao">Observação</label>
            <input class="form-control" type="text" name="observacao" value="<?=$edit['observacao']?>" required>
        </div>


        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>
