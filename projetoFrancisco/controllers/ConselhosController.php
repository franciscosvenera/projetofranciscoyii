<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhosModel;
use yii;

class ConselhosController extends Controller{
    public function actionListarConselhos(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = ConselhosModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $conselhos = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-conselhos',[
            'conselhos' => $conselhos,
            'paginacao' => $paginacao,
        ]);
        
    }
    public function actionCadastrarConselhos(){
        return $this->render('cadastrar-conselhos');
    }

    public function actionRealizaCadastroConselho(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new ConselhosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['conselhos/listar-conselhos']);
        }
        return $this->render('cadastrar-conselhos');
    }

    public static function listaConselhossSelect(){
        $query = ConselhosModel::find();

        return $query->orderBy('nome')->all();
    }

    public function actionEditarConselhos(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = ConselhosModel::find();
            $conselhos = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-conselhos',[
        'edit' => $conselhos
        ]);
      }
  
    public function actionRealizaEdicaoConselho(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = ConselhosModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['conselhos/listar-conselhos',
                  'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'conselhos/listar-conselhos']
                ]);
              }else{
                  return $this->redirect(['conselhos/listar-conselhos', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaConselho(){
        
        try {
            $request = \yii::$app->request;
            $model = ConselhosModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['conselhos/listar-conselhos', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'conselhos/listar-conselhos'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['conselhos/listar-conselhos', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'conselhos/listar-conselhos'
                ]
            ]);  
        }

    }
}
?>