<?
namespace app\modules\api\controllers;

use app\models\AdministradorasModel;
use Exception;
use yii\web\Controller;

class AdministradorasController extends Controller{

    public function actionGetAll(){
        $qry = AdministradorasModel::find();
        $data = $qry->orderBy('nomeAdm')->all();
        $dados = [];
        $i = 0;

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeAdm'] = $d['nomeAdm'];
                $dados['resultSet'][$i]['CNPJ'] = $d['CNPJ'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);

    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = AdministradorasModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['resultSet'][0]['id'] = $d['id'];
            $dados['resultSet'][0]['nomeAdm'] = $d['nomeAdm'];
            $dados['resultSet'][0]['CNPJ'] = $d['CNPJ'];

        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }   

        return json_encode($dados);
    }
    public function actionGetTokenPost(){
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if($fieldName && $tokenValue){
            return $fieldName.':'.$tokenValue;
        }else{
            return false;
        } 
    }

    public function actionRegisterAdm(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new AdministradorasModel();
                $model->attributes = $request->post();
                if ($model->save()){
                    $dados['endPoint']['status'] = 'success';
                    $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';
                }else{
                    $dados['endPoint']['status'] = 'noData';
                    $dados['endPoint']['msg'] = 'Não foi possível executar esta operação.';
                }
                
                return json_encode($dados);
            }

        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            
            return json_encode($dados);
        }

    }
    public function actionEditAdm(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = AdministradorasModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro';
        }
    }

    public function actionDeleteAdm(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = AdministradorasModel::findOne($request->post('id'));
                $model->delete();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $e;
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro';
        }
    }
}