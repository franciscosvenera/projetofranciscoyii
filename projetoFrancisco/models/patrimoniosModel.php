<?
namespace app\models;

use yii\db\ActiveRecord;

class PatrimoniosModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_patrimonios';
    }
    public function rules(){
        return [
            [['descricao', 'data_aquisicao', 'quantidade', 'valor', 'documento', 'observacao', 'idCondominio'], 'required']
        ];
    }
}
?>