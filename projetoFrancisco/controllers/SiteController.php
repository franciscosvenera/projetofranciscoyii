<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller{
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionHome(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('home');
    }

    public function actionIndex(){
        return $this->render('home');
    }

    public function actionLogin(){
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = \yii::$app->request;

        if($request->isPost){
            $identity = LoginForm::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
            if($identity){
                Yii::$app->user->login($identity);
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['login', 
                'myAlert' => 
                [
                    'type' => 'warning', 
                    'msg' => 'Dados de usuário não conferem',
                    'redir' => 'index.php?r=site/login'
                ]
                ]);
            }
        }
        return $this->render('login');
    }
    /**
     * @return Response
     */
    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }

}
