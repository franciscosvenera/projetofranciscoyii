<?
namespace app\models;

use yii\db\ActiveRecord;

class MoradoresModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_moradores';
    }
    public function rules(){
        return [
            [['nome', 'cpf', 'email', 'fone', 'idCondominio', 'idBloco', 'idUnidade', 'dataNascimento'], 'required']
        ];
    }
}
?>