<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use yii;

class BlocosController extends Controller{
    public function actionListarBlocos(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        //$query = BlocosModel::find();
        $blTable = BlocosModel::tableName();
        $condTable = CondominiosModel::tableName();
        $query = (new \yii\db\Query())
        ->select(
            'bl.id, 
            bl.nomeBloco,
            bl.andares, 
            bl.unidadesAndar, 
            cond.nomeCondominio, 
            bl.idCondominio'

        )
        ->from($blTable.' bl')
        ->innerJoin($condTable.' cond', 'cond.id = bl.idCondominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $blocos = $query->orderBy('nomeBloco')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-blocos',[
            'blocos' => $blocos,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastrarBlocos(){
        return $this->render('cadastrar-blocos');
    }

    public function actionRealizaCadastroBloco(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new BlocosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['blocos/listar-blocos']);
        }
        return $this->render('cadastrar-blocos');
    }

    public static function listaBlocosSelect(){
        $query = BlocosModel::find();

        return $query->orderBy('nomeBloco')->all();
    }

    public static function novolistaBlocosSelect($from){
        $query = BlocosModel::find();
        $data = $query->where(['idCondominio' => $from])->orderBy('nomeBloco')->all();
        return $data;
    }

    public function actionListBlocosApi(){
        $request = \yii::$app->request;
        $query=BlocosModel::find();
        $data = $query->where(['idCondominio' => $request->post()])->orderBy('nomeBloco')->all();

        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeBloco'] = $d['nomeBloco'];
            $i++;
        }
        return json_encode($dados);
    }
    public function actionEditarBlocos(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = BlocosModel::find();
            $blocos = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-blocos',[
        'edit' => $blocos
        ]);
      }
  
      public function actionRealizaEdicaoBloco(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = BlocosModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['blocos/listar-blocos']);
              }else{
                  return $this->redirect(['blocos/listar-blocos', 'msg' => 'erro']);
              }
          }
    }

    public function actionDeletaBloco(){
        
        try {
            $request = \yii::$app->request;
            $model = BlocosModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['blocos/listar-blocos', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'blocos/listar-blocos'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['blocos/listar-blocos', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'blocos/listar-blocos'
                ]
            ]);  
        }

    }
}
?>