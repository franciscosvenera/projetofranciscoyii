<?
namespace app\components;

use yii\base\Component;

class selectedComponents extends Component{

    public static function isSelected($id, $from){
        return ($id == $from) ? 'selected' : '';
    }
}
?>