<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdministradorasModel;
use app\components\alertComponent;
use yii;

class AdministradorasController extends Controller{
    public function actionListarAdministradoras(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        $query = AdministradorasModel::find();

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $administradoras = $query->orderBy('nomeAdm')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-administradoras',[
            'administradoras' => $administradoras,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastrarAdministradoras(){
        return $this->render('cadastrar-administradoras');
    }

    public function actionRealizaCadastroAdministradora(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AdministradorasModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['administradoras/listar-administradoras']);
        }
        return $this->render('cadastrar-administradoras');
    }

    public static function listaAdministradorasSelect(){
        $query = AdministradorasModel::find();

        return $query->orderBy('nomeAdm')->all();
    }

    public function actionEditarAdministradoras(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-administradoras',[
        'edit' => $administradoras
        ]);
      }
  
      public function actionRealizaEdicaoAdministradora(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = AdministradorasModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['administradoras/listar-administradoras',
                  'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'administradoras/listar-administradoras']
                ]);
              }else{
                  return $this->redirect(['administradoras/listar-administradoras', 'msg' => 'erro']);
              }
          }
      }

    public function actionDeletaAdministradora(){
        
        try {
            $request = \yii::$app->request;
            $model = AdministradorasModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['administradoras/listar-administradoras', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'administradoras/listar-administradoras'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['administradoras/listar-administradoras', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'administradoras/listar-administradoras'
                ]
            ]);  
        }

    }

}
?>