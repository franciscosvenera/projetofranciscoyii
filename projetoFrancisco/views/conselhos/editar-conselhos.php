<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<h1>Cadastra Conselheiros</h1>
<form method="post" id="cadastroConselho" action="<?= Url::to(['conselhos/realiza-edicao-conselho']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="<?=$edit['nome']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">CPF</label>
            <input class="form-control" type="text" name="cpf" value="<?=$edit['cpf']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">E-mail</label>
            <input class="form-control" type="text" name="email" value="<?=$edit['email']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Fone</label>
            <input class="form-control" type="text" name="fone" value="<?=$edit['fone']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Função</label>
            <input class="form-control" type="text" name="funcao" value="<?=$edit['funcao']?>" required>
        </div>
        
        
        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>