<?
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\maskComponents;
use app\components\alertComponents;
use app\components\modalComponents;

$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponents::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'],'?r='.$_GET['myAlert']['redir']);
}

?>
<h1>Administradoras</h1>

<table class="col col-12 table table-striped mt-5" id="listaAdministradoras">
    <tr>
        <td>Nome</td>
        <td>CNPJ</td>
        <td>DATA</td>
        <td align="center"><a href="index.php?r=administradoras/cadastrar-administradoras" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($administradoras as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nomeAdm'] ?></td>
            <td><?= maskComponents::mask($dados['CNPJ'],'CNPJ') ?></td>
            <td><?= Yii::$app->formatter->format($dados['dataCadastro'],'date') ?></td>
            <td align="center">
                <a href="index.php?r=administradoras/editar-administradoras&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=$url_site?>/index.php?r=administradoras/deleta-administradora&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>
    <?=modalComponents::initModal();?>