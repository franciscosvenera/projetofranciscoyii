<?
namespace app\models;

use yii\db\ActiveRecord;

class ConselhosModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_conselhos';
    }

    public function rules(){
        return [
            [['nome', 'cpf', 'email', 'fone', 'funcao'], 'required']
        ];
    }
}
?>