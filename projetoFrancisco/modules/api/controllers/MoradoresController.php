<?
namespace app\modules\api\controllers;

use app\models\MoradoresModel;
use Exception;
use yii\web\Controller;

class MoradoresController extends Controller{

    public function actionGetAll(){
        $qry = MoradoresModel::find();
        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i = 0;

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);

    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = MoradoresModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;

            }

        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }   

        return json_encode($dados);
    }

    public function actionGetTokenPost(){
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if($fieldName && $tokenValue){
            return $fieldName.':'.$tokenValue;
        }else{
            return false;
        } 
    }

    public function actionRegisterMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new MoradoresModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';
    
                return json_encode($dados);
            }

        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
            
            return json_encode($dados);
        }

    }
    public function actionEditMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro';
        }
    }

    public function actionDeleteMorador(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = MoradoresModel::findOne($request->post('id'));
                $model->delete();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $e;
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro';
        }
    }
}