-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para fv_sistema
CREATE DATABASE IF NOT EXISTS `fv_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fv_sistema`;

-- Copiando estrutura para tabela fv_sistema.fv_administradoras
CREATE TABLE IF NOT EXISTS `fv_administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(50) DEFAULT NULL,
  `CNPJ` varchar(14) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_administradoras: ~2 rows (aproximadamente)
INSERT INTO `fv_administradoras` (`id`, `nomeAdm`, `CNPJ`, `dataCadastro`) VALUES
	(9, 'Trend Administradora', '94403096000182', NULL),
	(10, 'A Moradia Administradora', '73812934000100', NULL);

-- Copiando estrutura para tabela fv_sistema.fv_blocos
CREATE TABLE IF NOT EXISTS `fv_blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `andares` varchar(11) NOT NULL DEFAULT '',
  `unidadesAndar` varchar(255) NOT NULL DEFAULT '',
  `idCondominio` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Bloco_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Bloco_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_blocos: ~4 rows (aproximadamente)
INSERT INTO `fv_blocos` (`id`, `nomeBloco`, `andares`, `unidadesAndar`, `idCondominio`) VALUES
	(11, 'A', '4', '4', 14),
	(12, 'B', '6', '4', 14),
	(13, 'Torre 5', '4', '6', 16),
	(14, 'Torre 6', '6', '6', 16),
	(15, 'Bloco único', '6', '2', 17);

-- Copiando estrutura para tabela fv_sistema.fv_condominios
CREATE TABLE IF NOT EXISTS `fv_condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtde` int(11) NOT NULL DEFAULT 0,
  `logradouro` varchar(50) NOT NULL DEFAULT '',
  `numero` varchar(50) NOT NULL DEFAULT '',
  `bairro` varchar(50) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `uf` varchar(50) NOT NULL DEFAULT '',
  `cep` varchar(8) NOT NULL DEFAULT '',
  `sindico` int(11) NOT NULL DEFAULT 0,
  `idAdmin` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Condominio_Sindico` (`sindico`),
  KEY `FK_Condominio_Admin` (`idAdmin`),
  CONSTRAINT `FK_Condominio_Admin` FOREIGN KEY (`idAdmin`) REFERENCES `fv_administradoras` (`id`),
  CONSTRAINT `FK_Condominio_Sindico` FOREIGN KEY (`sindico`) REFERENCES `fv_conselhos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_condominios: ~1 rows (aproximadamente)
INSERT INTO `fv_condominios` (`id`, `nomeCondominio`, `qtde`, `logradouro`, `numero`, `bairro`, `cidade`, `uf`, `cep`, `sindico`, `idAdmin`) VALUES
	(14, 'Condominio das Hortensias', 5, 'Rua Áustria', '100', 'Nações', 'Indaial', 'SC', '89082256', 19, 9),
	(16, 'Condominio das Rosas', 10, 'Rua França', '200', 'Nações', 'Indaial', 'SC', '89082256', 18, 9),
	(17, 'Condominio das Orquídeas', 4, 'Rua França', '500', 'Nações', 'Indaial', 'SC', '89082256', 20, 9);

-- Copiando estrutura para tabela fv_sistema.fv_conselhos
CREATE TABLE IF NOT EXISTS `fv_conselhos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `funcao` enum('Subsindico','Conselho fiscal') NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Conselho_Condominio` (`idCondominio`) USING BTREE,
  CONSTRAINT `FK_Conselho_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_conselhos: ~4 rows (aproximadamente)
INSERT INTO `fv_conselhos` (`id`, `nome`, `cpf`, `email`, `fone`, `funcao`, `idCondominio`) VALUES
	(18, 'José da Silva', '123.456.789', 'josedasilva@gmail.com', '47988998989', '', NULL),
	(19, 'João dos Santtos', '605.490.890', 'joaosantos@gmail.com', '47988888888', '', NULL),
	(20, 'Maria Simão', '666.829.010', 'mariasimao@gmail.com', '47988998985', 'Conselho fiscal', NULL),
	(21, 'Manoel Simão', '465.347.020', 'manoelsimao@gmail.com', '4733338989', 'Conselho fiscal', NULL);

-- Copiando estrutura para tabela fv_sistema.fv_moradores
CREATE TABLE IF NOT EXISTS `fv_moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataNascimento` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Morador_Bloco` (`idBloco`),
  KEY `FK_Morador_Condominio` (`idCondominio`),
  KEY `FK_Morador_Unidade` (`idUnidade`),
  CONSTRAINT `FK_Morador_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_blocos` (`id`),
  CONSTRAINT `FK_Morador_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`),
  CONSTRAINT `FK_Morador_Unidade` FOREIGN KEY (`idUnidade`) REFERENCES `fv_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_moradores: ~0 rows (aproximadamente)
INSERT INTO `fv_moradores` (`id`, `nome`, `cpf`, `email`, `fone`, `idCondominio`, `idBloco`, `idUnidade`, `dataUpdate`, `dataCadastro`, `dataNascimento`) VALUES
	(29, 'Pedro Henrique Joaquim Henrique Moreira', '66682901010', 'pedromoreira@construtore.com.br', '6339397096', 14, 11, 13, '2022-05-10 00:24:20', '2022-05-10 00:24:20', '0000-00-00 00:00:00'),
	(30, ' Antonella Luzia Isabel Alves', '19276700910', 'ant@mundivox.com.br', ' 47995944626', 16, 12, 14, '2022-05-10 00:33:35', '2022-05-10 00:33:35', '2022-05-10 00:33:35'),
	(31, ' Raul Manoel da Paz', '51294292955', ' raul@moyageorges.com.br', ' 48987832009', 14, 12, 13, '2022-05-10 00:33:41', '2022-05-10 00:33:41', '2022-05-10 00:33:41'),
	(32, ' Larissa Allana Drumond', '239.593.089', ' larissa@ornatopresentes.com.br', ' 48987697610', 16, 13, 15, '2022-05-10 00:35:29', '2022-05-10 00:35:29', '0000-00-00 00:00:00'),
	(33, ' Daniela Isabella Ester Sales', '991.190.109', ' daniela@salvadorlogistica.com.br', ' 48992288416', 16, 14, 16, '2022-05-10 00:36:52', '2022-05-10 00:36:52', '0000-00-00 00:00:00'),
	(34, ' Mariana Brenda Larissa Gonçalves', '914.500.889', ' mariana@konekoshouten.com.br', ' 4837221303', 16, 14, 16, '2022-05-10 00:44:39', '2022-05-10 00:44:39', '0000-00-00 00:00:00'),
	(35, ' Bryan Levi Julio Moura', '926.733.649', 'bryan-moura80@hotmal.com', ' 48981105742', 17, 15, 17, '2022-05-10 00:51:48', '2022-05-10 00:51:48', '0000-00-00 00:00:00');

-- Copiando estrutura para tabela fv_sistema.fv_patrimonios
CREATE TABLE IF NOT EXISTS `fv_patrimonios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `data_aquisicao` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `quantidade` int(11) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `documento` varchar(50) NOT NULL,
  `observacao` varchar(255) NOT NULL,
  `idCondominio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patriminio_condominio` (`idCondominio`),
  CONSTRAINT `FK_patriminio_condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.fv_patrimonios: ~0 rows (aproximadamente)
INSERT INTO `fv_patrimonios` (`id`, `descricao`, `data_aquisicao`, `quantidade`, `valor`, `documento`, `observacao`, `idCondominio`) VALUES
	(1, 'Computador', '2022-01-01 02:00:00', 5, 1000, '123456', 'Computador novo', 14),
	(2, 'Elevador', '2022-02-01 02:00:00', 1, 60000, '555444', 'Elevador torre B', 16);

-- Copiando estrutura para tabela fv_sistema.fv_unidades
CREATE TABLE IF NOT EXISTS `fv_unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroUnidade` int(50) NOT NULL DEFAULT 0,
  `metragem` float NOT NULL DEFAULT 0,
  `vagasGaragem` int(50) NOT NULL DEFAULT 0,
  `idBloco` int(50) NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Unidade_Bloco` (`idBloco`),
  KEY `FK_Unidade_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Unidade_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_blocos` (`id`),
  CONSTRAINT `FK_Unidade_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_unidades: ~4 rows (aproximadamente)
INSERT INTO `fv_unidades` (`id`, `numeroUnidade`, `metragem`, `vagasGaragem`, `idBloco`, `idCondominio`) VALUES
	(13, 101, 55, 2, 11, 14),
	(14, 102, 55, 2, 11, 14),
	(15, 500, 100, 2, 13, 16),
	(16, 600, 100, 2, 14, 16),
	(17, 100, 120, 2, 15, 17);

-- Copiando estrutura para tabela fv_sistema.fv_usuarios
CREATE TABLE IF NOT EXISTS `fv_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(50) NOT NULL DEFAULT '',
  `senha` varchar(50) NOT NULL DEFAULT '',
  `datacadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.fv_usuarios: ~0 rows (aproximadamente)
INSERT INTO `fv_usuarios` (`id`, `nome`, `usuario`, `senha`, `datacadastro`) VALUES
	(15, 'francisco', 'francisco', '123', '2022-05-09 00:06:19');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
