<?
namespace app\controllers;

use app\models\AdministradorasModel;
use app\models\CondominiosModel;
use app\models\MoradoresModel;
use yii\base\Controller;

Class ConsultasController extends Controller{
    static function getCondMoradores(){
        $query = (new \yii\db\Query())
       ->select('
       cad.idCondominio,
       cond.nomeCondominio,
       COUNT(cad.id) AS Total
       ')
        ->FROM (MoradoresModel::tableName().' cad')
        ->LEFTJOIN (CondominiosModel::tableName().' cond','cad.idCondominio = cond.id')
        ->groupBy(['cad.idCondominio'])
        ->all();
        return ($query);
    }
    static function getLastAdm(){
        $query = (new \yii\db\Query())
        ->select('nomeAdm')
        ->from(AdministradorasModel::tableName())
        ->orderBy(['dataCadastro' => SORT_DESC])->limit(5)
        ->All();
        return $query;
    }
}
?>