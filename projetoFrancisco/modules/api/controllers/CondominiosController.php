<?
namespace app\modules\api\controllers;

use app\models\CondominiosModel;
use Exception;
use yii\web\Controller;

class CondominiosController extends Controller{

    public function actionGetAll(){
        $qry = CondominiosModel::find();
        $data = $qry->orderBy('nomeCondominio')->all();
        $dados = [];
        $i = 0;

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);

    }

    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = CondominiosModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;

            }

        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }   

        return json_encode($dados);
    }

    public function actionGetTokenPost(){
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if($fieldName && $tokenValue){
            return $fieldName.':'.$tokenValue;
        }else{
            return false;
        } 
    }

    public function actionRegisterCond(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = new CondominiosModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido com sucesso.';
    
                return json_encode($dados);
            }

        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = $th;
            
            return json_encode($dados);
        }

    }
    public function actionEditCond(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = CondominiosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não foi possível editar o registro';
        }
    }

    public function actionDeleteCond(){
        $request = \yii::$app->request;

        try {
            if($request->isPost){
                $model = CondominiosModel::findOne($request->post('id'));
                $model->delete();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                
                return json_encode($dados);
            }
        } catch (Exception $e) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $e;
            $dados['endPoint']['msg'] = 'Não foi possível deletar o registro';
        }
    }

    public function actionGetCondominioFromAdm(){
        $request = \yii::$app->request;
        $qry = CondominiosModel::find();

        $data = $qry->where(['idAdmin' => $request->get('idAdmin')])->orderBy('nomeCondominio')->all();
        $dados = [];

        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeCondominio'] = $d['nomeCondominio'];
                $i++;
            }
        }else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }
}