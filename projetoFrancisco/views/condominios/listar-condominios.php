<?

use app\components\alertComponents;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\maskComponents;
use app\components\modalComponents;

$url_site = Url::base($schema = true);
if(isset($_GET['msg'])){
    echo alertComponents::myAlert('danger','Erro na requisição');
}

?>
<h1>Condominios</h1>

<table class="col col-12 table table-striped mt-5" id="listaCondominios">
    <tr>
        <td>Condominio</td>
        <td>Blocos</td>
        <td>Logradouro</td>
        <td>Numero</td>
        <td>Bairro</td>
        <td>Cidade</td>
        <td>UF</td>
        <td>CEP</td>
        <td>Sindico</td>
        <td>Administradora</td>
        
        <td align="center"><a href="index.php?r=condominios/cadastrar-condominios" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($condominios as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nomeCondominio'] ?></td>
            <td><?= $dados['qtde'] ?></td>
            <td><?= $dados['logradouro'] ?></td>
            <td><?= $dados['numero'] ?></td>
            <td><?= $dados['bairro'] ?></td>
            <td><?= $dados['cidade'] ?></td>
            <td><?= $dados['uf'] ?></td>
            <td><?= maskComponents::mask($dados['cep'],'cep') ?></td>
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['nomeAdm'] ?></td>
            <td align="center">
                <a href="index.php?r=condominios/editar-condominios&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=$url_site?>/index.php?r=condominios/deleta-condominio&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>
    <?=modalComponents::initModal();?>