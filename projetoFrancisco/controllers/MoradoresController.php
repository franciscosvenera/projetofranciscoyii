<?
namespace app\controllers;

use app\components\alertComponents;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use yii;

class MoradoresController extends Controller{
    public function actionListarMoradores(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        //$query = MoradoresModel::find();
        $uniTable = UnidadesModel::tableName();
        $blTable = BlocosModel::tableName();
        $condTable = CondominiosModel::tableName();
        $moradorTable = MoradoresModel::tableName();
        $query = (new \yii\db\Query())
        ->select(
        'cad.id, 
        cad.nome, 
        cad.cpf, 
        cad.email, 
        cad.fone, 
        cad.dataCadastro, 
        cond.nomeCondominio, 
        bl.nomeBloco, 
        uni.numeroUnidade, 
        cad.idCondominio, 
        cad.idBloco, 
        cad.idUnidade,
        cad.dataUpdate,
        cad.dataCadastro'
        )

        ->from($moradorTable.' cad')
        ->innerJoin($uniTable.' uni', 'uni.id = cad.idUnidade')
        ->innerJoin($condTable.' cond', 'cond.id = uni.idCondominio')
        ->innerJoin($blTable. ' bl', 'bl.id = uni.idBloco');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $moradores = $query->orderBy('nome')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-moradores',[
            'moradores' => $moradores,
            'paginacao' => $paginacao,
        ]);
    }
    public function actionCadastrarMoradores(){
        return $this->render('cadastrar-moradores');
    }

    public function actionRealizaCadastroMorador(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new MoradoresModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['moradores/listar-moradores']);
        }
        return $this->render('cadastrar-moradores');
    }

    public function actionEditarMoradores(){
      $this->layout = false;
      $request = \yii::$app->request;
      if($request->isGet){
          $query = MoradoresModel::find();
          $moradores = $query->where(['id' =>$request->get()])->one();
      }
      return $this->render('editar-moradores',[
      'edit' => $moradores
      ]);
    }

    public function actionRealizaEdicaoMorador(){

        $request = \yii::$app->request;
        if($request->isPost){

            $model = MoradoresModel::findOne($request->post('id'));
            $model->attributes = $request->post();
            
            if($model->update()){
                return $this->redirect(['moradores/listar-moradores',
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'moradores/listar-moradores']
            ]);
            }else{
                return $this->redirect(['moradores/listar-moradores', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaMorador(){
        
        try {
            $request = \yii::$app->request;
            $model = MoradoresModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['moradores/listar-moradores', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'moradores/listar-moradores'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['moradores/listar-moradores', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'moradores/listar-moradores'
                ]
            ]);  
        }

    }

}
?>