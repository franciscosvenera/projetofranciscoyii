<?

use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<h1>Cadastrar Patrimônio</h1>
<form method="post" id="cadastroCondominio" action="<?echo Url::to(['patrimonios/realiza-cadastro-patrimonio']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="idCondominio">Condominio</label>
            <select name="idCondominio" class="custom-select">
                <? 
                foreach(CondominiosController::listaCondominiosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeCondominio']?></option>
                <? } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="descricao">Descrição</label>
            <input class="form-control" type="text" name="descricao" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="data_aquisicao">Data Aquisição</label>
            <input class="form-control" type="date" name="data_aquisicao" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="quantidade">Quantidade</label>
            <input class="form-control" type="text" name="quantidade" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="valor">Valor</label>
            <input class="form-control" type="tel" name="valor" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="documento">Documento</label>
            <input class="form-control" type="text" name="documento" value="" required>
        </div>
        <div class="col-12 col-md-12">
            <div class="form-group">
                <label for="">Observação</label>
                <textarea class="form-control" name="observacao" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
        </div>


        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>
