<?
namespace app\models;

use yii\db\ActiveRecord;

class CondominiosModel extends ActiveRecord{

    public static function tableName(){
        return 'fv_condominios';
    }

    public function rules(){
        return [ 
        [['nomeCondominio', 'qtde', 'idAdmin'], 'required'],
        [['logradouro', 'numero', 'bairro', 'cidade', 'uf', 'cep', 'sindico'], 'default']

        ];
    }
}
?>