<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<h1>Cadastra Conselheiros</h1>
<form method="post" id="cadastroConselho" action="<?= Url::to(['conselhos/realiza-cadastro-conselho']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">CPF</label>
            <input class="form-control" type="text" name="cpf" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">E-mail</label>
            <input class="form-control" type="text" name="email" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Fone</label>
            <input class="form-control" type="text" name="fone" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Função</label>
            <input class="form-control" type="text" name="funcao" value="" required>
        </div>
        
        
        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>