<?

use app\components\alertComponents;
use app\components\modalComponents;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$url_site = Url::base($schema = true);

if(isset($_GET['msg'])){
    echo alertComponents::myAlert('danger','Erro na requisição');
}

?>
<h1>Conselheiros</h1>

<table class="col col-12 table table-striped mt-5" id="listaConselhos">
    <tr>
        <td>Nome</td>
        <td>CPF</td>
        <td>E-mail</td>
        <td>Fone</td>
        <td>Função</td>
        
        <td align="center"><a href="index.php?r=conselhos/cadastrar-conselhos" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? foreach ($conselhos as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['cpf'] ?></td>
            <td><?= $dados['email'] ?></td>
            <td><?= $dados['fone'] ?></td>
            <td><?= $dados['funcao'] ?></td>
            
            <td align="center">
                <a href="index.php?r=conselhos/editar-conselhos&id=<?=$dados['id']?>" class="openModal"><i class="bi bi-pencil-square"></i></a>
                <a href="<?=$url_site?>/index.php?r=conselhos/deleta-conselho&id=<?=$dados['id']?>" <i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
        <? } ?>
        
    </table>

    <?= LinkPager::widget([
        'pagination' => $paginacao,
        'linkContainerOptions' => [
            'class' => 'page-item'
        ],
        'linkOptions' =>[
            'class' => 'page-link'
        ],
        'disabledListItemSubTagOptions' => [
            'class' => 'pagelink'
        ]
    ])?>

    <div class="row">
        <div class="totalRegistros col-sm-6">Total Registros <?=$paginacao->totalCount?></div>
    </div>
    <?=modalComponents::initModal();?>