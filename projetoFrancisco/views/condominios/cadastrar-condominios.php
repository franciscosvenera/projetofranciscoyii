<?

use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\controllers\ConselhosController;
use app\components\estadosComponents;
?>

<h1>Cadastrar Condomínio</h1>
<form method="post" id="cadastroCondominio" action="<?echo Url::to(['condominios/realiza-cadastro-condominio']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="idAdmin">Administradora</label>
            <select name="idAdmin" class="custom-select">
                <? 
                foreach(AdministradorasController::listaAdministradorasSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeAdm']?></option>
                <? } ?>
            </select>
        </div>

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nomeCondominio" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="qtde">Qtde. Blocos</label>
            <input class="form-control" type="text" name="qtde" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="logradouro">Logradouro</label>
            <input class="form-control" type="text" name="logradouro" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="numero">Nr.</label>
            <input class="form-control" type="text" name="numero" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="bairro">Bairro</label>
            <input class="form-control" type="text" name="bairro" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="cidade">Cidade</label>
            <input class="form-control" type="text" name="cidade" value="" required>
        </div>
        <div class="col-sm-6 form-group">
            <label for="uf">UF</label>
            <select name="uf" class="form-control fromCondominio">
                <option selected>Selecione...</option>
                <? foreach (estadosComponents::estados() as $sig => $uf) { ?>
                    <option value="<?= $sig?>"><?=$uf?></option>
                <? } ?>
            </select>    
        </div>


        <div class="col-sm-6 form-group">
            <label for="cep">CEP</label>
            <input class="form-control" type="text" name="cep" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="sindico">Sindico</label>
            <select name="sindico" class="custom-select">
                <? 
                foreach(ConselhosController::listaConselhossSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nome']?></option>
                <? } ?>
            </select>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>
