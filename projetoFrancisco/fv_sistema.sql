-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para fv_sistema
DROP DATABASE IF EXISTS `fv_sistema`;
CREATE DATABASE IF NOT EXISTS `fv_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fv_sistema`;

-- Copiando estrutura para tabela fv_sistema.fv_administradoras
DROP TABLE IF EXISTS `fv_administradoras`;
CREATE TABLE IF NOT EXISTS `fv_administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(50) DEFAULT NULL,
  `CNPJ` varchar(14) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_blocos
DROP TABLE IF EXISTS `fv_blocos`;
CREATE TABLE IF NOT EXISTS `fv_blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `andares` varchar(11) NOT NULL DEFAULT '',
  `unidadesAndar` varchar(255) NOT NULL DEFAULT '',
  `idCondominio` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Bloco_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Bloco_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_condominios
DROP TABLE IF EXISTS `fv_condominios`;
CREATE TABLE IF NOT EXISTS `fv_condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtde` int(11) NOT NULL DEFAULT 0,
  `logradouro` varchar(50) NOT NULL DEFAULT '',
  `numero` varchar(50) NOT NULL DEFAULT '',
  `bairro` varchar(50) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `uf` varchar(50) NOT NULL DEFAULT '',
  `cep` varchar(8) NOT NULL DEFAULT '',
  `sindico` int(11) NOT NULL DEFAULT 0,
  `idAdmin` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Condominio_Sindico` (`sindico`),
  KEY `FK_Condominio_Admin` (`idAdmin`),
  CONSTRAINT `FK_Condominio_Admin` FOREIGN KEY (`idAdmin`) REFERENCES `fv_administradoras` (`id`),
  CONSTRAINT `FK_Condominio_Sindico` FOREIGN KEY (`sindico`) REFERENCES `fv_conselhos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_conselhos
DROP TABLE IF EXISTS `fv_conselhos`;
CREATE TABLE IF NOT EXISTS `fv_conselhos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `funcao` enum('Subsindico','Conselho fiscal') NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Conselho_Condominio` (`idCondominio`) USING BTREE,
  CONSTRAINT `FK_Conselho_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_moradores
DROP TABLE IF EXISTS `fv_moradores`;
CREATE TABLE IF NOT EXISTS `fv_moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Morador_Bloco` (`idBloco`),
  KEY `FK_Morador_Condominio` (`idCondominio`),
  KEY `FK_Morador_Unidade` (`idUnidade`),
  CONSTRAINT `FK_Morador_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_blocos` (`id`),
  CONSTRAINT `FK_Morador_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`),
  CONSTRAINT `FK_Morador_Unidade` FOREIGN KEY (`idUnidade`) REFERENCES `fv_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_patrimonios
DROP TABLE IF EXISTS `fv_patrimonios`;
CREATE TABLE IF NOT EXISTS `fv_patrimonios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `data_aquisicao` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `quantidade` int(11) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `documento` varchar(50) NOT NULL,
  `observacao` varchar(255) NOT NULL,
  `idCondominio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_patriminio_condominio` (`idCondominio`),
  CONSTRAINT `FK_patriminio_condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_unidades
DROP TABLE IF EXISTS `fv_unidades`;
CREATE TABLE IF NOT EXISTS `fv_unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroUnidade` int(50) NOT NULL DEFAULT 0,
  `metragem` float NOT NULL DEFAULT 0,
  `vagasGaragem` int(50) NOT NULL DEFAULT 0,
  `idBloco` int(50) NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Unidade_Bloco` (`idBloco`),
  KEY `FK_Unidade_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Unidade_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_blocos` (`id`),
  CONSTRAINT `FK_Unidade_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para tabela fv_sistema.fv_usuarios
DROP TABLE IF EXISTS `fv_usuarios`;
CREATE TABLE IF NOT EXISTS `fv_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(50) NOT NULL DEFAULT '',
  `senha` varchar(50) NOT NULL DEFAULT '',
  `datacadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Exportação de dados foi desmarcado.

-- Copiando estrutura para view fv_sistema.vw_consulta_bloco
DROP VIEW IF EXISTS `vw_consulta_bloco`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_bloco` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_cadastro
DROP VIEW IF EXISTS `vw_consulta_cadastro`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_cadastro` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_condominio
DROP VIEW IF EXISTS `vw_consulta_condominio`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_condominio` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_unidade
DROP VIEW IF EXISTS `vw_consulta_unidade`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_unidade` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_convidados_festa
DROP VIEW IF EXISTS `vw_convidados_festa`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_convidados_festa` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_moradores_por_condominio
DROP VIEW IF EXISTS `vw_moradores_por_condominio`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_moradores_por_condominio` 
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_bloco
DROP VIEW IF EXISTS `vw_consulta_bloco`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_bloco`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_bloco` AS SELECT 
bl.id, bl.nomeBloco, bl.andares, bl.unidadesAndar, cond.nomeCondominio, bl.idCondominio
FROM
fv_bloco bl 
INNER JOIN fv_condominios cond ON bl.idCondominio = cond.id
ORDER BY cond.nomeCondominio ;

-- Copiando estrutura para view fv_sistema.vw_consulta_cadastro
DROP VIEW IF EXISTS `vw_consulta_cadastro`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_cadastro`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_cadastro` AS SELECT
cad.id, cad.nome, cad.cpf, cad.email, cad.fone, cad.dataCadastro, cond.nomeCondominio, bl.nomeBloco, uni.numeroUnidade, cad.idCondominio, cad.idBloco, cad.idUnidade 
FROM 
fv_cadastro cad
LEFT JOIN fv_unidade uni ON cad.idUnidade = uni.id
LEFT JOIN fv_bloco bl ON cad.idBloco = bl.id
LEFT JOIN fv_condominios cond ON cad.idCondominio = cond.id ;

-- Copiando estrutura para view fv_sistema.vw_consulta_condominio
DROP VIEW IF EXISTS `vw_consulta_condominio`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_condominio`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_condominio` AS SELECT
cond.id, cond.nomeCondominio, cond.qtde, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.uf, cond.cep, con.nome, adm.nomeAdm, cond.sindico, cond.idAdmin
FROM
fv_condominios cond
LEFT JOIN fv_conselho con ON cond.sindico = con.id
LEFT JOIN fv_administradora adm ON cond.idAdmin = adm.id ;

-- Copiando estrutura para view fv_sistema.vw_consulta_unidade
DROP VIEW IF EXISTS `vw_consulta_unidade`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_unidade`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_unidade` AS SELECT 
uni.id, 
uni.numeroUnidade, 
uni.metragem,
uni.vagasGaragem, 
bl.nomeBloco, 
cond.nomeCondominio, 
uni.idBloco, 
uni.idCondominio
FROM
fv_unidade uni
LEFT JOIN fv_bloco bl ON uni.idBloco = bl.id
LEFT JOIN fv_condominios cond ON uni.idCondominio = cond.id ;

-- Copiando estrutura para view fv_sistema.vw_convidados_festa
DROP VIEW IF EXISTS `vw_convidados_festa`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_convidados_festa`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_convidados_festa` AS SELECT festa.id, festa.tituloEvento, festa.idUnidade, festa.dataHoraEvento, un.numeroUnidade, cad.nome,
 
(SELECT GROUP_CONCAT(' ',lista.convidado)  FROM lfv_lista_convidados lista WHERE idReservaSalao = festa.id)
FROM fv_reserva_salao_festa festa
INNER JOIN fv_unidade un ON festa.idUnidade = un.id
LEFT JOIN fv_cadastro cad ON cad.idUnidade = un.id ;

-- Copiando estrutura para view fv_sistema.vw_moradores_por_condominio
DROP VIEW IF EXISTS `vw_moradores_por_condominio`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_moradores_por_condominio`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_moradores_por_condominio` AS SELECT
cond.nomeCondominio,
COUNT(cad.id) AS Total
FROM 
fv_cadastro cad
LEFT JOIN fv_condominios cond ON cad.idCondominio = cond.id
GROUP BY cad.idCondominio ;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
