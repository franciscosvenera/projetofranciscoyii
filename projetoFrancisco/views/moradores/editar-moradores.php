<?

use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\controllers\BlocosController;
use app\controllers\UnidadesController;
use kartik\datetime\DateTimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use app\components\selectedComponents;

?>

<h1>Cadastra Moradores</h1>
<form method="post" id="cadastroMorador" action="<?= Url::to(['moradores/realiza-edicao-morador']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="<?=$edit['nome']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cpf">CPF</label>
            <input class="form-control" type="text" name="cpf" value="<?=$edit['cpf']?>" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cpf">Nascimento</label>
            <?
            echo DatePicker::widget([
                'bsVersion' => '4',
                'name' => 'dataNascimento',
                'type' => DateTimePicker::TYPE_INPUT,
                'value' => '01-01-1980',
                'language' => 'pt',
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose'=>true,
                    'format' => 'dd-M-yyyy'
                ]
            ]);
            ?>
        </div>

        <div class="col-sm-6 form-group">
            <label for="email">E-mail</label>
            <input class="form-control" type="text" name="email" value="<?=$edit['email']?>" required>
        </div>

        <div class="col-sm-3 form-group">
            <label for="fone">Fone</label>
            <input class="form-control" type="text" name="fone" value="<?=$edit['fone']?>" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select idCondominio">
                <? 
                foreach(CondominiosController::listaCondominiosSelect() as $cond){?>
                <option value="<?=$cond['id']?>"<?=selectedComponents::isSelected($cond['id'], $edit['idCondominio'])?>><?=$cond['nomeCondominio']?></option>
            
                <? } ?>
            </select>
        </div>
        

        <div class="col-sm-4 form-group">
            <label for="idBloco">Bloco</label>
            <select name="idBloco" class="idBloco custom-select">
                <?
                foreach(BlocosController::novolistaBlocosSelect($edit['idCondominio']) as $bloco){
                    echo '<option value="'.$bloco['id'].'"'.selectedComponents::isSelected($bloco['id'], $edit['idBloco']).'>'.$bloco['nomeBloco'].'</option>';
                }
                ?>
            </select>
        </div>

        <div class="col-sm-4 form-group">
            <label for="idUnidade">Unidade</label>
            <select name="idUnidade" class="fromUnidade custom-select">
                <?
                foreach(UnidadesController::listaUnidadesSelect($edit['idCondominio']) as $und){
                    echo '<option value="'.$und['id'].'"'.selectedComponents::isSelected($und['id'], $edit['idUnidade']).'>'.$und['numeroUnidade'].'</option>';
                }
                ?>
            </select>
        </div>

        <div class="col-sm-12 form-group">
            <?
            echo FileInput::widget([
                'name' => 'files',
                'attribute' => 'files[]',
                'options' => ['multiple' => true]
            ]);
            ?>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <input type="hidden" name="id" value="<?=$edit['id']?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
</form>
