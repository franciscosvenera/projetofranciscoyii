<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
?>

<h1>Cadastra blocos</h1>
<form method="post" id="cadastroBloco" action="<?= Url::to(['blocos/realiza-cadastro-bloco']);?>">
    <div class="form-row">

        <div class="col-sm-6 form-group">
            <label for="nome">Bloco</label>
            <input class="form-control" type="text" name="nomeBloco" value="" required>
        </div>
        
        <div class="col-sm-6 form-group">
            <label for="cnpj">Andares</label>
            <input class="form-control" type="text" name="andares" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="cnpj">Unid. por Andar</label>
            <input class="form-control" type="text" name="unidadesAndar" value="" required>
        </div>

        <div class="col-sm-6 form-group">
            <label for="idCondominio">Condomínio</label>
            <select name="idCondominio" class="custom-select">
                <? 
                foreach(CondominiosController::listaCondominiosSelect() as $adm){?>
                <option value="<?=$adm['id']?>"><?=$adm['nomeCondominio']?></option>
                <? } ?>
            </select>
        </div>

        <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="col-sm-12">
            <button type="submit" class="btn btn-info buttonEnviar">Enviar</button>
        </div>
    </div>
</form>