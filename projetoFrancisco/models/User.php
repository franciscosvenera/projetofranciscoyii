<?
namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{
    public static function tableName()
    {
        return 'fv_usuarios';
    }

    /**
     * @param [type] $id
     * @return void
     */

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    /**
     * @param [type] $token
     * @param [type] $type
     * @return void
     */

     public static function findIdentityByAccessToken($token, $type = null){
         return null; //static::findOne(['token' => $token]);
     }

     /**
      * @return void
      */

      public function getId(){
          return $this->id;
      }

      /**
       * @return void
       */
      public function getAuthKey()
      {
          return null; //$this->auth_key;
      }

      /**
       * @param [type] $auth_key
       * @return void
       */
      public function validateAuthKey($auth_key){
          return null;//$this->auth_key === $authKey;
      }
}
?>