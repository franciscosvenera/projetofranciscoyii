<?
namespace app\controllers;

use yii;
use yii\base\Controller;

class userLogado extends Controller{

    public function isLogado(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
    }
}
?>