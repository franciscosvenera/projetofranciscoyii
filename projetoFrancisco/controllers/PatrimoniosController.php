<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\PatrimoniosModel;
use app\models\CondominiosModel;
use yii;

class PatrimoniosController extends Controller{

    public function actionListarPatrimonios(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        //$query = MoradoresModel::find();
   
        $condTable = CondominiosModel::tableName();
        $patrimonioTable = PatrimoniosModel::tableName();
        $query = (new \yii\db\Query())
        ->select(
        'cad.id, 
        cad.descricao, 
        cad.data_aquisicao, 
        cad.quantidade, 
        cad.valor, 
        cad.documento,  
        cad.observacao,
        cond.nomeCondominio'
        )

        ->from($patrimonioTable.' cad')
        ->innerJoin($condTable.' cond', 'cond.id = cad.idCondominio');

        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $patrimonios = $query->orderBy('descricao')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-patrimonios',[
            'patrimonios' => $patrimonios,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastrarPatrimonios(){
        return $this->render('cadastrar-patrimonios');
    }

    public function actionRealizaCadastroPatrimonio(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new PatrimoniosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['patrimonios/listar-patrimonios']);
        }
        return $this->render('cadastrar-patrimonios');
    }

    public static function listaPatrimoniosSelect(){
        $query = PatrimoniosModel::find();

        return $query->orderBy('descricao')->all();
    }

    public function actionEditarPatrimonios(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = PatrimoniosModel::find();
            $patrimonios = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-patrimonios',[
        'edit' => $patrimonios
        ]);
      }
  
      public function actionRealizaEdicaoPatrimonio(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = PatrimoniosModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['patrimonios/listar-patrimonios',
                  'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'patrimonios/listar-patrimonios']
                ]);
              }else{
                  return $this->redirect(['patrimonios/listar-patrimonios', 'msg' => 'erro']);
              }
          }
      }

    public function actionDeletaPatrimonio(){
        
        try {
            $request = \yii::$app->request;
            $model = PatrimoniosModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['patrimonios/listar-patrimonios', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'patrimonios/listar-patrimonios'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['patrimonios/listar-patrimonios', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'patrimonios/listar-patrimonios'
                ]
            ]);  
        }

    }

}
?>