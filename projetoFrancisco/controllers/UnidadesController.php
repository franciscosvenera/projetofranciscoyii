<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadesModel;
use app\models\BlocosModel;
use app\models\CondominiosModel;
use yii;

class UnidadesController extends Controller{
    public function actionListarUnidades(){

        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        //$query = UnidadesModel::find();
        $uniTable = UnidadesModel::tableName();
        $blTable = BlocosModel::tableName();
        $condTable = CondominiosModel::tableName();
        $query = (new \yii\db\Query())
        ->select(
        'uni.id, 
        uni.numeroUnidade, 
        uni.metragem,
        uni.vagasGaragem, 
        bl.nomeBloco, 
        cond.nomeCondominio, 
        uni.idBloco, 
        uni.idCondominio'
        )

        ->from($uniTable.' uni')
        ->innerJoin($condTable.' cond', 'cond.id = uni.idCondominio')
        ->innerJoin($blTable. ' bl', 'bl.id = uni.idBloco');


        $paginacao = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $unidades = $query->orderBy('numeroUnidade')
        ->offset($paginacao->offset)
        ->limit($paginacao->limit)
        ->all();
        return $this->render('listar-unidades',[
            'unidades' => $unidades,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastrarUnidades(){
        return $this->render('cadastrar-unidades');
    }

    public function actionRealizaCadastroUnidade(){

        $request = \yii::$app->request;

        if($request->isPost){
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/listar-unidades']);
        }
        return $this->render('cadastrar-unidades');
    }

    public static function listaUnidadesSelect(){
        $query = UnidadesModel::find();

        return $query->orderBy('numeroUnidade')->all();
    }
    public function actionListUnidadesApi(){
        $request = \yii::$app->request;
        $query=UnidadesModel::find();
        $data = $query->where(['idBloco' => $request->post()])->orderBy('numeroUnidade')->all();

        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['numeroUnidade'] = $d['numeroUnidade'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditarUnidades(){
        $this->layout = false;
        $request = \yii::$app->request;
        if($request->isGet){
            $query = UnidadesModel::find();
            $unidades = $query->where(['id' =>$request->get()])->one();
        }
        return $this->render('editar-unidades',[
        'edit' => $unidades
        ]);
      }
  
    public function actionRealizaEdicaoUnidade(){
  
          $request = \yii::$app->request;
          if($request->isPost){
  
              $model = UnidadesModel::findOne($request->post('id'));
              $model->attributes = $request->post();
              
              if($model->update()){
                  return $this->redirect(['unidades/listar-unidades',
                  'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro editado com sucesso.',
                    'redir' => 'unidades/listar-unidades']
                ]);
              }else{
                  return $this->redirect(['unidades/listar-unidades', 'msg' => 'erro']);
            }
        }
    }

    public function actionDeletaUnidade(){
        
        try {
            $request = \yii::$app->request;
            $model = UnidadesModel::findOne($request->get('id'));
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['unidades/listar-unidades', 
                'myAlert' => [
                    'type' => 'success', 
                    'msg' => 'Registro deletado.',
                    'redir' => 'unidades/listar-unidades'
                ]
            ]);
        
        } catch (\Throwable $error) {
            return $this->redirect(['unidades/listar-unidades', 
                'myAlert' => 
                [
                    'type' => 'danger', 
                    'msg' => 'Não foi possível deletar o registro',
                    'redir' => 'unidades/listar-unidades'
                ]
            ]);  
        }

    }
}
?>